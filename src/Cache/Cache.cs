﻿using System.Collections.Concurrent;
using Caching.Algorithms;

namespace Caching
{
    public class Cache<T,TU>
    {
        private ConcurrentDictionary<int, TU>[] _cacheSets;
        private readonly int _numberOfSets;
        private readonly ICacheAlgorithm _algorithm;

        public Cache(ICacheAlgorithm algorithm, int numberOfSets)
        {
            _algorithm = algorithm;
            _numberOfSets = numberOfSets;
            InitializeCacheSets(numberOfSets);
        }

        private void InitializeCacheSets(int numberOfSets)
        {
            _cacheSets = new ConcurrentDictionary<int, TU>[_numberOfSets];
            for (var i = 0; i < numberOfSets; i++)
            {
                _cacheSets[i] = new ConcurrentDictionary<int, TU>();
            }
        }

        public void Store(T key, TU value)
        {
            var internalKey = GetKey(key);
            var cache = GetCacheFor(internalKey);
            _algorithm.Apply(cache);
            cache.AddOrUpdate(internalKey, value, (k, v) => value);
        }

        private ConcurrentDictionary<int, TU> GetCacheFor(int internalKey)
        {
            var set = NaiveSetDistribution.DetermineFor(internalKey, _numberOfSets);
            return _cacheSets[set];
        }

        private static int GetKey(T key)
        {
            return key.GetHashCode();
        }

        public TU Retrieve(T key)
        {
            TU value;
            var internalKey = GetKey(key);
            var cache = GetCacheFor(internalKey);
            if (cache.TryGetValue(internalKey, out value))
            {
                _algorithm.CacheItemRetrieved(internalKey);
                return value;
            }

            throw new CacheItemNotFoundException("cache item: " + internalKey + " not found");
        }
    }
}

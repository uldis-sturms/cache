﻿using System;

namespace Caching
{
    public class NaiveSetDistribution
    {
        public static int DetermineFor(int internalKey, int numberOfSets)
        {
            return Math.Abs(internalKey % numberOfSets);
        }
    }
}
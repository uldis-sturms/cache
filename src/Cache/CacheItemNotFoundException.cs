﻿using System;

namespace Caching
{
    public class CacheItemNotFoundException : ApplicationException
    {
        public CacheItemNotFoundException(string message) : base(message)
        {
        }
    }
}
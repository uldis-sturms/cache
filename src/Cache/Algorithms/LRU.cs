﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Caching.Algorithms
{
    public class LRU : ICacheAlgorithm
    {
        private readonly int _maxSize;
        private readonly ConcurrentDictionary<int, DateTimeOffset> _retrieves = new ConcurrentDictionary<int, DateTimeOffset>();

        public LRU(int maxSize)
        {
            _maxSize = maxSize;
        }

        public void Apply<T>(ConcurrentDictionary<int, T> cache)
        {
            if (cache.Count == _maxSize)
            {
                var key = GetFirstNotUsedOrLeastRecentlyUsedKey(cache);
                T value;
                cache.TryRemove(key, out value);
            }
        }

        private int GetFirstNotUsedOrLeastRecentlyUsedKey<T>(ConcurrentDictionary<int, T> cache)
        {
            var firstNotUsedCacheItemKey = FirstNotUsedCacheItemKey(cache);
            if (firstNotUsedCacheItemKey != default(int))
            {
                return firstNotUsedCacheItemKey;
            }

            return LeastRecentlyUsedKey();
        }

        private int FirstNotUsedCacheItemKey<T>(ConcurrentDictionary<int, T> cache)
        {
            return cache.Keys.FirstOrDefault(key => !_retrieves.ContainsKey(key));
        }

        private int LeastRecentlyUsedKey()
        {
            var leastRecentUse = DateTimeOffset.UtcNow;
            int leastRecentUsedKey = default(int);
            foreach (var retrieve in _retrieves)
            {
                if (retrieve.Value < leastRecentUse)
                {
                    leastRecentUsedKey = retrieve.Key;
                    leastRecentUse = retrieve.Value;
                }
            }

            return leastRecentUsedKey;
        }

        public void CacheItemRetrieved(int key)
        {
            _retrieves.AddOrUpdate(key, DateTimeOffset.UtcNow, (k, v) => DateTimeOffset.UtcNow);
        }
    }
}
﻿using System;
using System.Collections.Concurrent;

namespace Caching.Algorithms
{
    public class MRU : ICacheAlgorithm
    {
        private readonly int _maxSize;
        private readonly ConcurrentDictionary<int, DateTimeOffset> _retrieved = new ConcurrentDictionary<int, DateTimeOffset>();

        public MRU(int maxSize)
        {
            _maxSize = maxSize;
        }

        public void Apply<T>(ConcurrentDictionary<int, T> cache)
        {
            if (cache.Count == _maxSize)
            {
                var mostRecentlyUsedKey = GetMostRecentlyUsedKey();

                T value;
                cache.TryRemove(mostRecentlyUsedKey, out value);
            }
        }

        private int GetMostRecentlyUsedKey()
        {
            var mostRecentUse = DateTimeOffset.MinValue;
            int mostRecentlyUsedKey = default(int);

            foreach (var retrieve in _retrieved)
            {
                if (retrieve.Value > mostRecentUse)
                {
                    mostRecentUse = retrieve.Value;
                    mostRecentlyUsedKey = retrieve.Key;
                   
                }
            }

            return mostRecentlyUsedKey;
        }

        public void CacheItemRetrieved(int key)
        {
            _retrieved.AddOrUpdate(key, DateTimeOffset.UtcNow, (k, v) => DateTimeOffset.UtcNow);
        }
    }
}
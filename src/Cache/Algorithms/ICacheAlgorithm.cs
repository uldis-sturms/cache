using System.Collections.Concurrent;

namespace Caching.Algorithms
{
    public interface ICacheAlgorithm
    {
        void Apply<T>(ConcurrentDictionary<int, T> cache);
        void CacheItemRetrieved(int key);
    }
}
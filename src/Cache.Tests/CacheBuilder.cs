﻿using Caching.Algorithms;
using Caching.Tests.Algorithms;

namespace Caching.Tests
{
    class CacheBuilder<T, TU>
    {
        private ICacheAlgorithm _algorithm = new NoEvictionAlgorithm();
        private int _numberOfSets = 1;

        public Cache<T, TU> Build()
        {
            return new Cache<T, TU>(_algorithm, _numberOfSets);
        }

        public CacheBuilder<T, TU> WithAlgorithm(ICacheAlgorithm algorithm)
        {
            _algorithm = algorithm;
            return this;
        }

        public CacheBuilder<T, TU> WithSets(int numberOfSets)
        {
            _numberOfSets = numberOfSets;
            return this;
        }
    }
}
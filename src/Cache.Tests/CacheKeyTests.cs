﻿using NUnit.Framework;

namespace Caching.Tests
{
    class CacheKeyTests
    {
        private const string _expected = "value";
        private const string _notExpected = "not-expected-value";

        [Test]
        public void allows_keys_to_be_strings()
        {
            const string key = "key";
            var cache = new CacheBuilder<string, string>().Build();

            StoreExpected(cache, key);
            StoreNotExpected(cache, "not-expected");

            RetrievedAsExpected(cache.Retrieve(key));
        }

        [Test]
        public void allows_keys_to_be_ints()
        {
            const int key = 1;
            var cache = new CacheBuilder<int, string>().Build();

            StoreExpected(cache, key);
            StoreNotExpected(cache, 2);

            RetrievedAsExpected(cache.Retrieve(key));
        }

        [Test]
        public void allows_keys_to_be_classes()
        {
            var key = new TestClass { TestProperty = "key"};
            var cache = new CacheBuilder<TestClass, string>().Build();

            StoreExpected(cache, key);
            StoreNotExpected(cache, new TestClass { TestProperty = "not-expected"});

            RetrievedAsExpected(cache.Retrieve(key));
        }

        private static void StoreExpected<T>(Cache<T, string> cache, T key)
        {
            cache.Store(key, _expected);
        }

        private static void RetrievedAsExpected(string retrieved)
        {
            Assert.That(retrieved, Is.EqualTo(_expected));
        }

        private void StoreNotExpected<T>(Cache<T, string> cache, T key)
        {
            cache.Store(key, _notExpected);
        }
    }
}

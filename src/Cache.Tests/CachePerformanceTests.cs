﻿using System;
using System.Diagnostics;
using Caching.Algorithms;
using NUnit.Framework;

namespace Caching.Tests
{
    class CachePerformanceTests
    {
        private Cache<int, int> _cache;
        private const int _maxSize = 10000;

        [Test]
        public void first_store_in_cache_should_be_fast()
        {
            LRUCacheWithMaxSizeOf(_maxSize);

            var time = Time(() => StoreInCache(_maxSize));

           Assert.That(time.TotalMilliseconds, Is.LessThan(400));
        }

        [Test]
        public void updates_in_cache_should_be_fast()
        {
            LRUCacheWithMaxSizeOf(_maxSize);

            StoreInCache(_maxSize);

            var time = Time(() => StoreInCache(_maxSize));

           Assert.That(time.TotalMilliseconds, Is.LessThan(400));
        }

        [Test]
        public void retrieves_from_cache_should_be_fast()
        {
            LRUCacheWithMaxSizeOf(_maxSize);

            StoreInCache(_maxSize);

            var time = Time(() => RetrieveFromCache(_maxSize));

           Assert.That(time.TotalMilliseconds, Is.LessThan(400));
        }

        private void LRUCacheWithMaxSizeOf(int maxSize)
        {
            _cache = new CacheBuilder<int, int>()
                .WithAlgorithm(new LRU(maxSize))
                .Build();
        }

        private TimeSpan Time(Action action)
        {
            var sw = new Stopwatch();
            sw.Start();

            action();

            sw.Stop();
            return TimeSpan.FromTicks(sw.ElapsedTicks);
        }

        private void StoreInCache(int maxSize)
        {
            for (var i = 0; i < maxSize; i++)
            {
                _cache.Store(i, i);
            }
        }

        private void RetrieveFromCache(int maxSize)
        {
            for (int i = 0; i < maxSize; i++)
            {
                _cache.Retrieve(i);
            }
        }
    }
}

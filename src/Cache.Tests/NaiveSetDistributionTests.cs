﻿using NUnit.Framework;

namespace Caching.Tests
{
    class NaiveSetDistributionTests
    {
        [TestCase(-1, 1)]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 2)]
        [TestCase(3, 0)]
        public void equal_distribution_among_keys_for_3_set_cache(int key, int expectedSet)
        {
            var set = DetermineSetForCacheWithThreeSets(key);
            Assert.That(set, Is.EqualTo(expectedSet));
        }

        private int DetermineSetForCacheWithThreeSets(int key)
        {
            return NaiveSetDistribution.DetermineFor(key, 3);
        }
    }
}

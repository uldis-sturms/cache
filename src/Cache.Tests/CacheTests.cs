﻿using NUnit.Framework;

namespace Caching.Tests
{
    public class CacheTests
    {
        private Cache<string, string> _cache;

        [TestCase("some-key", "some-value")]
        [TestCase("another-key", "another-value")]
        public void stores_value_in_cache(string key, string value)
        {
            given_a_cache();

            when_value_is_stored(key, value);

            then_retrieved_should_be_as_expected(key, value);
        }

        [Test]
        public void updates_value_in_cache()
        {
            const string key = "key";

            given_a_cache();

            when_value_is_stored(key, "value");
            when_value_is_stored(key, "updated-value");

            then_retrieved_should_be_as_expected(key, "updated-value");
        }

        [Test]
        public void throws_when_retrieving_by_not_present_key()
        {
            given_a_cache();

            then_should_throw_exception_for_non_existing_key();
        }

        private void given_a_cache()
        {
            _cache = new CacheBuilder<string, string>().Build();
        }

        private void when_value_is_stored(string key, string value)
        {
            _cache.Store(key, value);
        }

        private void then_retrieved_should_be_as_expected(string key, string value)
        {
            var retrieved = _cache.Retrieve(key);
            Assert.That(retrieved, Is.EqualTo(value));
        }

        private void then_should_throw_exception_for_non_existing_key()
        {
            Assert.Throws<CacheItemNotFoundException>(() => _cache.Retrieve("not-present-key"));
        }
    }
}

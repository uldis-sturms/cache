using System.Collections.Concurrent;
using Caching.Algorithms;

namespace Caching.Tests.Algorithms
{
    internal class NoEvictionAlgorithm : ICacheAlgorithm
    {
        public void Apply<T>(ConcurrentDictionary<int, T> cache)
        {
            
        }

        public void CacheItemRetrieved(int key)
        {

        }
    }
}
﻿using NUnit.Framework;

namespace Caching.Tests
{
    class CacheValueTests
    {
        private const string _key = "key";

        [Test]
        public void allows_values_to_be_strings()
        {
            const string value = "value";
            var cache = new CacheBuilder<string, string>().Build();

            StoreExpected(cache, value);

            RetrievedAsExpected(cache, value);
        }

        [Test]
        public void allows_values_to_be_ints()
        {
            const int value = 1;
            var cache = new CacheBuilder<string, int>().Build();

            StoreExpected(cache, value);

            RetrievedAsExpected(cache, value);
        }

        [Test]
        public void allows_values_to_be_classes()
        {
            var value = new TestClass { TestProperty = "key"};
            var cache = new CacheBuilder<string, TestClass>().Build();

            StoreExpected(cache, value);

            RetrievedAsExpected(cache, value);
        }

        private static void StoreExpected<T>(Cache<string, T> cache, T value)
        {
            cache.Store(_key, value);
        }

        private static void RetrievedAsExpected<T>(Cache<string, T> cache, T value)
        {
            Assert.That(cache.Retrieve(_key), Is.EqualTo(value));
        }
    }
}

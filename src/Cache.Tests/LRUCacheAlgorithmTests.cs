﻿using System;
using System.Threading;
using Caching.Algorithms;
using NUnit.Framework;

namespace Caching.Tests
{
    class LRUCacheAlgorithmTests
    {
        private static Cache<int, string> _cache;

        [Test]
        public void evicts_not_used_items_when_exceeds_max_size_for_the_same_set()
        {
            given_a_lru_cache_with_max_size(2);

            when_cache_item_is_stored(1, "value-1");
            when_cache_item_is_stored(11, "value-11");

            when_cache_is_retrieved_for_key(11);

            when_cache_item_is_stored(111, "value-111");

            then_throws_for_retrieving_evicted_cache_item_for(1);
        }

        [Test]
        public void evicts_least_recently_used_items_when_exceeds_max_size_for_the_same_set()
        {
            given_a_lru_cache_with_max_size(2);

            when_cache_item_is_stored(1, "value-1");
            when_cache_item_is_stored(11, "value-11");

            when_cache_is_retrieved_for_key(1);

            when_millisecond_has_passed();

            when_cache_is_retrieved_for_key(11);

            when_cache_item_is_stored(111, "value-111");

            then_throws_for_retrieving_evicted_cache_item_for(1);
        }

        [Test]
        public void updates_least_recently_used_items_when_exceeds_max_size_for_the_same_set()
        {
            given_a_lru_cache_with_max_size(2);

            when_cache_item_is_stored(1, "value-1");
            when_cache_item_is_stored(11, "value-11");

            when_cache_is_retrieved_for_key(1);
            when_cache_is_retrieved_for_key(1);

            when_millisecond_has_passed();

            when_cache_is_retrieved_for_key(11);

            when_cache_item_is_stored(111, "value-111");

            then_throws_for_retrieving_evicted_cache_item_for(1);
        }

        private void when_millisecond_has_passed()
        {
            Thread.Sleep(TimeSpan.FromMilliseconds(1));
        }

        private static void then_throws_for_retrieving_evicted_cache_item_for(int key)
        {
            Assert.Throws<CacheItemNotFoundException>(() => _cache.Retrieve(key));
        }

        private static void when_cache_is_retrieved_for_key(int key)
        {
            _cache.Retrieve(key);
        }

        private static void when_cache_item_is_stored(int key, string value)
        {
            _cache.Store(key, value);
        }

        private void given_a_lru_cache_with_max_size(int maxSize)
        {
            _cache = new CacheBuilder<int, string>()
                .WithAlgorithm(new LRU(maxSize))
                .WithSets(2)
                .Build();
        }
    }
}

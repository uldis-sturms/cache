﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Caching.Tests
{
    class CacheSetTests
    {
        private const int _numberOfSets = 3;
        private int _numberOfKeys = 4;

        [TestCaseSource("CacheSets")]
        public void stores_values_in_sets(int numberOfSets)
        {
            var cache = new CacheBuilder<int, int>()
                .WithSets(numberOfSets)
                .Build();

            for (var i = 0; i < _numberOfKeys; i++)
            {
                cache.Store(i, i);
            }

            for (var i = 0; i < _numberOfKeys; i++)
            {
                Assert.That(cache.Retrieve(i), Is.EqualTo(i));
            }
        }

        public static IEnumerable<int> CacheSets()
        {
            for (var i = 1; i <= _numberOfSets; i++)
            {
                yield return i;
            }
        }
    }
}
